FROM node:slim

WORKDIR /home/nihar/Desktop/Devops
COPY package.json /home/nihar/Desktop/Devops/
RUN npm install
COPY . /home/nihar/Desktop/Devops/
CMD node index.js
EXPOSE 4444
